#include <iostream>
#include <thread>
#include <atomic>
#include <mutex>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

using namespace std;

#define BUFFER_SIZE 100000U
#define PACKAGE_SIZE 4*1024

int local_port, remote_port;
char *remote_addr;

volatile bool endflags[UINT16_MAX];
mutex endlock;

void try_to_close(int client_fd, int proxy_fd) {
    endlock.lock();
    if(endflags[client_fd]) {
        endflags[client_fd] = false;
        close(client_fd);
        close(proxy_fd);
    } else endflags[client_fd] = true;
    endlock.unlock();
}

int wrrite(int fd, char buf[], int n) {
    int size = 0;
    while(size != n) {
        int m = write(fd, buf, n);
        if(m <= 0) return m;
        size += m;
    }
    return size;
}

void to_server(int client_fd, int proxy_fd) {
    char buf[BUFFER_SIZE];
    while(true) {
        int n = read(client_fd, buf, PACKAGE_SIZE);
        if(n <= 0) break;
        if(wrrite(proxy_fd, buf, n) <= 0) break;
    }
    shutdown(proxy_fd, SHUT_WR);
    shutdown(client_fd, SHUT_RD);
    try_to_close(client_fd, proxy_fd);
}

void to_client(int client_fd, int proxy_fd) {
    char buf[BUFFER_SIZE];
    while(true) {
        int n = read(proxy_fd, buf, PACKAGE_SIZE);
        if(n <= 0) break;
        if(wrrite(client_fd, buf, n) <= 0) break;
    }
    shutdown(proxy_fd, SHUT_RD);
    shutdown(client_fd, SHUT_WR);
    try_to_close(client_fd, proxy_fd);
}


int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
    if(argc != 4) exit(EXIT_FAILURE);
    sscanf(argv[1], "%d", &local_port);
    remote_addr = argv[2];
    sscanf(argv[3], "%d", &remote_port);

    int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(sock_fd < 0) {
        perror("socket");
        return 1;
    }

    sockaddr_in serveraddr, proxyaddr;

    bzero(&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serveraddr.sin_port = htons(local_port);

    bzero(&proxyaddr, sizeof(proxyaddr));
    proxyaddr.sin_family = AF_INET;
    proxyaddr.sin_addr.s_addr = inet_addr(remote_addr);
    proxyaddr.sin_port = htons(remote_port);

    if ( bind(sock_fd, (sockaddr*)(&serveraddr), sizeof(serveraddr)) < 0 ) {
        perror("bind");
        return 1;
    }
    if ( listen(sock_fd, 16) < 0 ) {
        perror("listen");
        return 1;
    }
    while ( true ) {
        int client_fd = accept(sock_fd, NULL, NULL);
        if(client_fd < 0) {
            perror("accept");
            continue;
        }
        int proxy_fd = socket(AF_INET, SOCK_STREAM, 0);
        if(proxy_fd < 0) {
            perror("proxy socket");
            close(client_fd);
            continue;
        }
        if(connect(proxy_fd, (sockaddr*)&proxyaddr, sizeof(proxyaddr)) != 0) {
            perror("proxy connect");
            close(proxy_fd);
            close(client_fd);
            continue;
        }
        thread(to_server, client_fd, proxy_fd).detach();
        thread(to_client, client_fd, proxy_fd).detach();
    }
    close(sock_fd);
}