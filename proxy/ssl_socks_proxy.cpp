#include <iostream>
#include <thread>
#include <atomic>
#include <mutex>

#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <openssl/ssl.h>
#include <openssl/err.h>

using namespace std;

#define BUFFER_SIZE 100000U
#define PACKAGE_SIZE 4*1024

int local_port;

volatile bool endflags[UINT16_MAX];
mutex endlock;

void init_openssl()
{ 
    SSL_load_error_strings();	
    OpenSSL_add_ssl_algorithms();
}

void cleanup_openssl()
{
    EVP_cleanup();
}

SSL_CTX *create_context()
{
    const SSL_METHOD *method;
    SSL_CTX *ctx;

    method = SSLv23_server_method();

    ctx = SSL_CTX_new(method);
    if (!ctx) {
	perror("Unable to create SSL context");
	ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    return ctx;
}

void configure_context(SSL_CTX *ctx)
{
    SSL_CTX_set_ecdh_auto(ctx, 1);

    /* Set the key and cert */
    if (SSL_CTX_use_certificate_file(ctx, "cert.pem", SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }

    if (SSL_CTX_use_PrivateKey_file(ctx, "key.pem", SSL_FILETYPE_PEM) <= 0 ) {
        ERR_print_errors_fp(stderr);
	exit(EXIT_FAILURE);
    }
}

void try_to_close(int client_fd, int proxy_fd, SSL *ssl) {
    endlock.lock();
    if(endflags[client_fd]) {
        endflags[client_fd] = false;
        SSL_free(ssl);
        close(client_fd);
        close(proxy_fd);
    } else endflags[client_fd] = true;
    endlock.unlock();
}

int wrrite(int fd, char buf[], int n) {
    int size = 0;
    while(size != n) {
        int m = write(fd, buf, n);
        if(m <= 0) return m;
        size += m;
    }
    return size;
}

int ssl_wrrite(SSL *ssl, char buf[], int n) {
    int size = 0;
    while(size != n) {
        int m = SSL_write(ssl, buf, n);
        if(m <= 0) return m;
        size += m;
    }
    return size;
}

void to_server(int client_fd, int proxy_fd, SSL *ssl, char first_bytes[], int first_n) {
    char buf[BUFFER_SIZE];
    char end = false;
    if(first_n > 0 && wrrite(proxy_fd, first_bytes, first_n) <= 0) end = true;
    if(!end) {
        while(true) {
            int n = SSL_read(ssl, buf, PACKAGE_SIZE);
            if(n <= 0) break;
            if(wrrite(proxy_fd, buf, n) <= 0) break;
        }
    }
    shutdown(proxy_fd, SHUT_WR);
    shutdown(client_fd, SHUT_RD);
    try_to_close(client_fd, proxy_fd, ssl);
}

void to_client(int client_fd, int proxy_fd, SSL *ssl) {
    char buf[BUFFER_SIZE];
    while(true) {
        int n = read(proxy_fd, buf, PACKAGE_SIZE);
        if(n <= 0) break;
        if(ssl_wrrite(ssl, buf, n) <= 0) break;
    }
    SSL_shutdown(ssl);
    shutdown(proxy_fd, SHUT_RD);
    shutdown(client_fd, SHUT_WR);
    try_to_close(client_fd, proxy_fd, ssl);
}

int read_addr_port(char buf[], string& addr, int& port) {
    int i = 0;
    while(buf[i] != ' ') addr += buf[i++];
    i++;
    port = 0;
    while(buf[i] != '\n') port = port * 10 + buf[i++] - '0';
    return i + 1;
}

int main(int argc, char *argv[]) {
    ios_base::sync_with_stdio(0);
    if(argc != 2) exit(EXIT_FAILURE);
    sscanf(argv[1], "%d", &local_port);

    int sock_fd = socket(AF_INET, SOCK_STREAM, 0);
    if(sock_fd < 0) {
        perror("socket");
        return 1;
    }

    sockaddr_in serveraddr, proxyaddr;

    bzero(&serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serveraddr.sin_port = htons(local_port);

    bzero(&proxyaddr, sizeof(proxyaddr));
    proxyaddr.sin_family = AF_INET;

    if ( bind(sock_fd, (sockaddr*)(&serveraddr), sizeof(serveraddr)) < 0 ) {
        perror("bind");
        return 1;
    }
    if ( listen(sock_fd, 16) < 0 ) {
        perror("listen");
        return 1;
    }


    SSL_CTX *ctx;
    init_openssl();
    ctx = create_context();
    configure_context(ctx);


    while ( true ) {
        int client_fd = accept(sock_fd, NULL, NULL);
        SSL *ssl;
        if(client_fd < 0) {
            perror("accept");
            continue;
        }
        ssl = SSL_new(ctx);
        SSL_set_fd(ssl, client_fd);
        if(SSL_accept(ssl) <= 0) {
            ERR_print_errors_fp(stderr);
            SSL_shutdown(ssl);
            SSL_free(ssl);
            close(client_fd);
            continue;
        }
        char buf[BUFFER_SIZE];
        int bytes = SSL_read(ssl, buf, BUFFER_SIZE);

        string remote_addr;
        int remote_port, start;
        start = read_addr_port(buf, remote_addr, remote_port);
        proxyaddr.sin_addr.s_addr = inet_addr(remote_addr.c_str());
        proxyaddr.sin_port = htons(remote_port);

        int proxy_fd = socket(AF_INET, SOCK_STREAM, 0);
        if(proxy_fd < 0) {
            perror("proxy socket");
            close(client_fd);
            continue;
        }
        if(connect(proxy_fd, (sockaddr*)&proxyaddr, sizeof(proxyaddr)) != 0) {
            perror("proxy connect");
            close(proxy_fd);
            close(client_fd);
            continue;
        }
        thread(to_server, client_fd, proxy_fd, ssl, buf + start, bytes - start).detach();
        thread(to_client, client_fd, proxy_fd, ssl).detach();
    }
    close(sock_fd);
    SSL_CTX_free(ctx);
    cleanup_openssl();
}