#include <sys/types.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <string.h>
#include <microhttpd.h>
#include <stdio.h>
#include <iostream>
#include <curl/curl.h>

using namespace std;

#define PORT 8888

string realUri;

static
void * logger(void * cls, const char * uri, struct MHD_Connection *con) {
     realUri = uri;
     return NULL;
 }

curl_slist *list = NULL;

size_t CurlWrite_CallbackFunc_StdString(void *contents, size_t size, size_t nmemb, string *s)
{
    size_t newLength = size * nmemb;
    try
    {
        s->append((char *)contents, newLength);
    }
    catch (std::bad_alloc &e)
    {
        return 0;
    }
    return newLength;
}


static int

print_out_key(void *cls, enum MHD_ValueKind kind, const char *key,
              const char *value)
{
    string header(key), val(value);
    list = curl_slist_append(list, (header + ": " + val).c_str());
    return MHD_YES;
}

static int
answer_to_connection(void *cls, MHD_Connection *connection,
                     const char *url, const char *method,
                     const char *version, const char *upload_data,
                     size_t *upload_data_size, void **con_cls)
{
    int ret;
    string response_string;
    MHD_Response *response;
    CURL *curl;
    curl = curl_easy_init();
    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING, "");
    curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method);
    curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, version);
    curl_easy_setopt(curl, CURLOPT_URL, realUri.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CurlWrite_CallbackFunc_StdString);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_string);
    MHD_get_connection_values(connection, MHD_HEADER_KIND, print_out_key,
                              cls);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
    curl_easy_perform(curl);

    response = MHD_create_response_from_buffer(response_string.size(), (void *)(response_string.c_str()), MHD_RESPMEM_MUST_COPY);
    ret = MHD_queue_response(connection, MHD_HTTP_OK, response);

    MHD_destroy_response(response);
    curl_slist_free_all(list);
    list = NULL;
    curl_easy_cleanup(curl);
    return ret;
}
int main()
{
    MHD_Daemon *daemon;
    daemon = MHD_start_daemon(MHD_USE_EPOLL_INTERNAL_THREAD, PORT, NULL, NULL,
                              &answer_to_connection, NULL, MHD_OPTION_URI_LOG_CALLBACK, &logger, NULL, MHD_OPTION_END);//my_logger, MHD_OPTION_END);
    if (NULL == daemon) {
        return 1;
    }

    cerr << "TYPE TO EXIT\n";
    getchar();
    MHD_stop_daemon(daemon);
    return 0;
}